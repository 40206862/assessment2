﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class guestClass
    {

     /*
     * Author: Drew Jamieson
     * Class for guests
     * Date last modified: 17/11/2016
     * 
     */

        public string name;
        public string passNo;
        public int age;
        int cost;
        int booking_ref = AutoIncrement.instance.booking_ref;
        int cust_ref = AutoIncrement.instance.cust_ref;

        //Find current directory and assign it to path
        string path = System.IO.Directory.GetCurrentDirectory();

        public guestClass(string nameIn, string passIn, int ageIn)
        {
            name = nameIn;
            passNo = passIn;
            age = ageIn;
        }

        public void writeGuest()
        {
            //Assign text to be written to variable text
            string text = name + Environment.NewLine + passNo + Environment.NewLine + age;

            //If cost.txt exists take the cost of the package from there, otherwise, set it to 0
            if (System.IO.Directory.Exists(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt"))
                cost = Convert.ToInt32(System.IO.File.ReadLines(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt"));
            else
                cost = 0;
            

            //Depending on age, add to the cost
            if (age < 19)
                cost += (Convert.ToInt32(System.IO.File.ReadLines(@path + "/" + cust_ref + "/" + booking_ref + "/Booking.txt").Skip(2).Take(1).First())) * 30;
            else if (age > 18)
                cost += (Convert.ToInt32(System.IO.File.ReadLines(@path + "/" + cust_ref + "/" + booking_ref + "/Booking.txt").Skip(2).Take(1).First())) * 50;
                
            //Create file to write to for individual booking references
            System.IO.Directory.CreateDirectory(@path + "/" +  cust_ref + "/" + booking_ref + "/guests");
            System.IO.File.WriteAllText(@path + "/" + cust_ref + "/" + booking_ref + "/guests/" + name + ".txt", text);
            
            if (System.IO.File.Exists(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt"))
            {
                //Reuse text to overwrite previous cost file
                String[] costText = new string[4];
                costText = System.IO.File.ReadAllLines(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt");
                costText[0] = Convert.ToString(cost);
                for (int i = 0; i == 4; i++)
                    costText[i] = "0";
                System.IO.File.WriteAllLines(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt", costText);
            }
            else
            {
                //If the file does not exist yet then create it.
                string[] costText = new string[4];
                costText[0] = Convert.ToString(cost);
                for (int i = 0; i == 4; i++)
                    costText[i] = "0";
                System.IO.File.WriteAllLines(@path + "/" + cust_ref + "/" + booking_ref + "/cost.txt", costText);
            }
        }
    }
}
