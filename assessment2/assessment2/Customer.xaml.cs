﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for choosing which of three options
     * Date last modified: 17/11/2016
     * 
     */
    public partial class Customer : Window
    {
        //Create new factory 
        Factory factory = new Factory();
        public bool ammend = false;
        //Set custRef to be the current customer reference number
        int custRef = AutoIncrement.instance.cust_ref;
        int bookRef;

        public Customer()
        {
            InitializeComponent();
            lblCustRef.Content = "Customer Reference Number: " + custRef;
        }
        
        public void Ammend(int custRefIn, int bookRefIn)
        {
            //If ammending, autofill the details and hide the save/quit buttons
            string path = System.IO.Directory.GetCurrentDirectory();

            custRef = custRefIn;
            bookRef = bookRefIn;

            txtName.Text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + custRefIn + "/cust.txt").Skip(0).Take(1).First());
            txtAddress.Text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + custRefIn + "/cust.txt").Skip(1).Take(1).First());

            btnQuit.Visibility = Visibility.Hidden;
            btnSave.Visibility = Visibility.Hidden;

            ammend = true;
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            //Do not save customer
            MessageBox.Show("No customer has been booked");
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //Save customer but do not continue to booking
            factory.addCustomer(txtName.Text, txtAddress.Text);
            this.Close();
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            
            //Save customer and continue to booking
            factory.addCustomer(txtName.Text, txtAddress.Text);
            AutoIncrement.instance.cust_ref = custRef;
            Booking frmBooking = new Booking();
            //If ammending then go into ammending of booking
            if (ammend)
            {
                MessageBox.Show("Please Ensure No Changes Are Required For Your Booking and Guest");
                frmBooking.ammended = true;
                frmBooking.ammend(custRef, bookRef);
            }
            this.Close();
            frmBooking.ShowDialog();
        }
    }
}
