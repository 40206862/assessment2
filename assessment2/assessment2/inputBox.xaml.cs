﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Forms;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * Acts as inputbox to get the booking or customer reference number
     * Date last modified: 17/11/2016
     * 
     */    

    public partial class inputBox : Window
    {
        public bool booking;
        public bool guest;
        public int custRef;
        public bool ammend = false;
        public bool guestAmmend = false;
        public bool custAmmend = false;

        public void booking_inputBox()
        {
            InitializeComponent();
            lblBooking.Visibility = System.Windows.Visibility.Hidden;
            guest = false;
            booking = true;
        }

        public void guest_inputBox()
        {
            InitializeComponent();
            lblCust.Visibility = System.Windows.Visibility.Hidden;
            guest = true;
            booking = false;
        }

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {



            //Get the user's cust/booking reference number and pass it to the relevant form
            string input = txtInput.Text;
            //Ensure txtInput isn't empty, and if booking is true
            if (!string.IsNullOrWhiteSpace(input) && booking)
            {
                //Ensure directory for entered input exists, i.e. the cust directory exists
                if (!System.IO.Directory.Exists((System.IO.Directory.GetCurrentDirectory() + "/" + input)))
                {
                    //If it doesn't exist, error
                    System.Windows.MessageBox.Show("This customer does not exist, please create new customer first");
                    base.Hide();
                }
                else
                //If it does exist
                {
                    //If guest is true
                    if (guest)
                    {
                        //Assign new customer reference number to what is entered in the textbox
                        AutoIncrement.instance.cust_ref = Convert.ToInt32(txtInput.Text);
                        //Create new inputbox tog et the booking reference number
                        inputBox bookingRef = new inputBox();
                        if (ammend)
                            bookingRef.ammend = true;
                        if (guestAmmend)
                            bookingRef.guestAmmend = true;
                        if (custAmmend)
                            bookingRef.custAmmend = true;
                        //Change the customer reference number to what has been entered
                        bookingRef.custRef = Convert.ToInt32(txtInput.Text);
                        bookingRef.guest_inputBox();
                        this.Hide();
                        bookingRef.ShowDialog();
                    }
                    else
                    // if guest is false
                    {
                        //If new booking is being added
                        //Assign cust_ref number to the number input
                        AutoIncrement.instance.cust_ref = Convert.ToInt32(txtInput.Text);
                        //If new booking then show new booking form
                        Booking frmBooking = new Booking();
                        base.Hide();
                        frmBooking.ShowDialog();
                    }
                }
            }
            //If booking is false, but guest is true and input has been entered
            else if (!string.IsNullOrWhiteSpace(input) && guest)
            {
                //Check that directory exists
                if (!System.IO.Directory.Exists((System.IO.Directory.GetCurrentDirectory() + "/" + custRef + "/" + input)))
                {
                    //If there no booking under the given custRef then error
                    System.Windows.MessageBox.Show("This booking does not exist under this customer, please create new booking.");
                    this.Close();
                    return;
                }
                else
                {
                    //If new guest then show new guest form
                    //Assign cust_ref number to the number input
                    AutoIncrement.instance.booking_ref = Convert.ToInt32(txtInput.Text);
                    if (guestAmmend)
                    {
                        Guest frmGuestAmmend = new Guest();
                        frmGuestAmmend.ammend(custRef, Convert.ToInt32(txtInput.Text));
                        this.Close();
                        frmGuestAmmend.ShowDialog();
                        return;
                    }
                    else if (custAmmend)
                    {
                        Customer frmCust = new Customer();
                        frmCust.Ammend(custRef, Convert.ToInt32(txtInput.Text));
                        this.Close();
                        frmCust.ShowDialog();
                        return;
                    }
                    else if (ammend)
                    {
                        Booking frmBooking = new Booking();
                        frmBooking.ammend(custRef, Convert.ToInt32(txtInput.Text));
                        this.Close();
                        frmBooking.ShowDialog();
                        return;
                    }
                    //If new booking then show new booking form
                    Guest frmGuest = new Guest();
                    base.Hide();
                    frmGuest.ShowDialog();
                }

            }
            else
                System.Windows.MessageBox.Show("Please ensure the box is not empty");
        }


        //Only allows number to be input
        private void txtInput_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                e.Handled = true;
            }
        }


        private void txtInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
