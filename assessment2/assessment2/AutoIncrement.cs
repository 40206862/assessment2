﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class AutoIncrement
    {
    /*
     * Author: Drew Jamieson
     * class for extras
     * Date last modified: 17/11/2016
     * Singleton class to hold autoincrementing numbers
     */
        
        private AutoIncrement() { }
        private static AutoIncrement Instance;
        public int booking_ref = 1;
        public int Booking_ref
        {
            get
            {
                //Search for bookingNumber.txt, if found assign number inside to booking ref, otherwise, initialise booking_ref
                if (System.IO.File.Exists(@System.IO.Directory.GetCurrentDirectory() + "/bookingNo.txt"))
                    booking_ref = (Convert.ToInt32(System.IO.File.ReadAllText(@System.IO.Directory.GetCurrentDirectory() + "/bookingNo.txt")) + 1);
                else
                    booking_ref = 1;
                return booking_ref;
            }
            set
            {
                //Search for bookingNumber.txt, if found assign number inside to booking ref, otherwise, initialise booking_ref
                if (System.IO.File.Exists(@System.IO.Directory.GetCurrentDirectory() + "/bookingNo.txt"))
                    booking_ref = (Convert.ToInt32(System.IO.File.ReadAllText(@System.IO.Directory.GetCurrentDirectory() + "/bookingNo.txt")) + 1);

                else
                    booking_ref = 1;
            }
        }

        public int cust_ref = 1;
        public int Cust_ref
        {
            get
            {
                //Search directory for custNumber.txt, if found assign contents, if not set 1
                if (System.IO.File.Exists(@System.IO.Directory.GetCurrentDirectory() + "/custNo.txt"))
                    cust_ref = (Convert.ToInt32(System.IO.File.ReadAllText(@System.IO.Directory.GetCurrentDirectory() + "/custNo.txt")) + 1);
                else
                    cust_ref = 1;
                return cust_ref;
            }
            set
            {
                //Search for custNumber.txt, if found assign number inside to booking ref, otherwise, initialise booking_ref
                if (System.IO.File.Exists(@System.IO.Directory.GetCurrentDirectory() + "/custNo.txt"))
                    cust_ref = (Convert.ToInt32(System.IO.File.ReadAllText(@System.IO.Directory.GetCurrentDirectory() + "/custNo.txt")) + 1);

                else
                    cust_ref = 1;
            }
        }

        public static AutoIncrement instance
        {
            get
            {
                //Create instance of AutoIncrement if one doesn't exist
                if (Instance == null)
                    Instance = new AutoIncrement();
                return Instance;
            }
        }

        
    }
}
