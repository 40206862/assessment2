﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.VisualBasic;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for choosing which of three options
     * Date last modified: 17/11/2016
     * 
     */
    public partial class NewChoice : Window
    {
        public bool ammend = false;
        public NewChoice()
        {
            InitializeComponent();
        }

        private void btnBooking_Click(object sender, RoutedEventArgs e)
        {
            //Open form for getting customer reference number
            inputBox custRef = new inputBox();
            
            custRef.booking_inputBox();
            if (ammend)
            {
                custRef.guest = true;
                custRef.ammend = true;
            }
            else
                AutoIncrement.instance.Booking_ref = 0;
            custRef.ShowDialog();
            this.Hide();
        }

        private void btnCust_Click(object sender, RoutedEventArgs e)
        {
            

            if (ammend)
            {
                inputBox custRef = new inputBox();
                custRef.custAmmend = true;
                custRef.ammend = true;
                
                custRef.booking_inputBox();
                custRef.guest = true; 
                custRef.ShowDialog();
                this.Hide();

            }
            else
            {
                //Open form to add a new customer
                AutoIncrement.instance.Cust_ref = 1;
                AutoIncrement.instance.Booking_ref = 1;
                Customer cust = new Customer();
                cust.ShowDialog();
            }
            
            this.Close();
        }

        private void btnGuest_Click(object sender, RoutedEventArgs e)
        {
            //Open a form to get the customer reference number
            inputBox custRef = new inputBox();
            if (ammend)
            {
                custRef.guestAmmend = true;
                custRef.ammend = true;
            }
            custRef.booking_inputBox();
            custRef.guest = true;; 
            custRef.ShowDialog();
            this.Hide();
        }
    }
}
