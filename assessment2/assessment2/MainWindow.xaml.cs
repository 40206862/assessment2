﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * Main GUI class
     * Date last modified: 17/11/2016
     * 
     */
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            //Open a choice form to decide which point to add
            NewChoice frmChoice = new NewChoice();
            frmChoice.ShowDialog();
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Open form for invoice
            invoiceChoice frmChoice = new invoiceChoice();
            frmChoice.ShowDialog();
        }

        private void btnExtras_Click(object sender, RoutedEventArgs e)
        {
            //open form for extras
            extras frmExtra = new extras();
            frmExtra.ShowDialog();
        }



        private void btnDelCust_Click(object sender, RoutedEventArgs e)
        {
            //Open form to delete customer
            Delete frmDelete = new Delete();
            frmDelete.DeleteCust();
            frmDelete.ShowDialog();
        }

        private void btnAmend_Click(object sender, RoutedEventArgs e)
        {
            //Open form to ammend
            NewChoice frmChoice = new NewChoice();
            frmChoice.ammend = true;
            frmChoice.ShowDialog();
        }

        private void btnDelBooking_Click(object sender, RoutedEventArgs e)
        {
            //Open form to delete booking
            Delete frmDelete = new Delete();
            frmDelete.ShowDialog();
        }
    }
}
