﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * class for extras
     * Date last modified: 17/11/2016
     * 
     */
    public partial class extraCars : Window
    {
        carClass car;
        Factory factory = new Factory();
        string path = System.IO.Directory.GetCurrentDirectory();

        public extraCars()
        {
            InitializeComponent();
        }

        public void addCar(carClass carIn)
        {
            car = carIn;
            //Set path to find original copies

            string text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + car.custRef + "/" + car.bookRef + "/Booking.txt").Skip(0).Take(1).First());

            //Autofill the comboboxes for what the user has previously entered
            string[] date = new string[3];
            int i = 0;
            foreach (string word in text.Split(' '))
            {
                date[i] = word;
                i++;
            }
            cmbFromDay.Text = date[0];
            cmbFromMonth.Text = date[1];
            cmbFromYear.Text = date[2];

            text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + car.custRef + "/" + car.bookRef + "/Booking.txt").Skip(1).Take(1).First());
            i = 0;
            foreach (string word in text.Split(' '))
            {
                date[i] = word;
                i++;
            }
            cmbToDay.Text = date[0];
            cmbToMonth.Text = date[1];
            cmbToYear.Text = date[2];

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("No Car has been booked.");
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            //Assign Toure and arrival dates
            car.hireTo = Convert.ToDateTime(Convert.ToInt32(cmbToDay.Text) + " " + Convert.ToString(cmbToMonth.Text) + " " + Convert.ToInt32(cmbToYear.Text));
            car.hireFrom = Convert.ToDateTime(Convert.ToInt32(cmbFromDay.Text) + " " + Convert.ToString(cmbFromMonth.Text) + " " + Convert.ToInt32(cmbFromYear.Text));
            
            //If the length of staying is equal to or less than 0 or hiring car for longer than they are staying
            if ((car.hireTo - car.hireFrom).Days <= 0 || ((car.hireTo - car.hireFrom).Days > (Convert.ToInt32(System.IO.File.ReadLines(@path + "/" + car.custRef + "/" + car.bookRef + "/Booking.txt").Skip(2).Take(1).First()))))
            {
                System.Windows.MessageBox.Show("This is not a valid date, please try again.");
                return;
            }
            else
            {
               car.daysHired =  (car.hireTo - car.hireFrom).Days;
            }
            //Create new booking, save booking and close form
            car.cost = car.daysHired * 50;
            string[] text = System.IO.File.ReadAllLines(@path + "/" + car.custRef + "/" + car.bookRef + "/cost.txt");
            text[1] = Convert.ToString(car.cost);
            System.IO.File.WriteAllLines(@path + "/" + car.custRef + "/" + car.bookRef + "/cost.txt", text);
            System.IO.File.WriteAllText(@path + "/" + car.custRef + "/" + car.bookRef + "/carHire.txt", txtDriver.Text + Environment.NewLine + car.daysHired);


            this.Close();
        }

        private void cmbAriveDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbToDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbAriveYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
