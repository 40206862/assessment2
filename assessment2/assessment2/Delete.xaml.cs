﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for deleting 
     * Date last modified: 17/11/2016
     *
     */
    public partial class Delete : Window
    {
        bool custDel = false;

        public Delete()
        {
            InitializeComponent();
            btnInvoice.Visibility = Visibility.Hidden;
        }

        public void DeleteCust()
        {
            InitializeComponent();
            lblBookRef.Visibility = Visibility.Hidden;
            txtBookRef.Visibility = Visibility.Hidden;
            custDel = true;
        }

        public void Invoice()
        {
            InitializeComponent();
            btnDel.Visibility = Visibility.Hidden;
            btnInvoice.Visibility = Visibility.Visible;
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        //Only allows number to be input
        private void txtInput_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                e.Handled = true;
            }
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            //If button quit entered make not changes
            MessageBox.Show("No changes have been made.");
            this.Close();
        }

        private void btnDel_Click(object sender, RoutedEventArgs e)
        {
            //If deleting the customer then check that txtCustRef is not empty
            if (custDel)
            {
                if (!string.IsNullOrWhiteSpace(txtCustRef.Text))
                {
                    //Create new factory and perform operation to delete a customer
                    Factory factory = new Factory();
                    factory.delCust(Convert.ToInt32(txtCustRef.Text));
                    this.Close();
                }
                else
                    MessageBox.Show("Please ensure neither box is empty.");
            }
            else
            //Deleting only the booking
            {
                if (!string.IsNullOrWhiteSpace(txtBookRef.Text) && !string.IsNullOrWhiteSpace(txtCustRef.Text))
                {
                    //Create new factory and perform operation to delete a booking
                    Factory factory = new Factory();
                    factory.delBooking(Convert.ToInt32(txtBookRef.Text), Convert.ToInt32(txtCustRef.Text));
                    this.Close();
                }
                else
                    MessageBox.Show("Please ensure neither box is empty.");
            }
        }

        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            //Get the invoice 

        }
    }
}
