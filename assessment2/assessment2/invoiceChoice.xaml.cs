﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for obtaining the invoice info
     * Date last modified: 17/11/2016
     * 
     */
    public partial class invoiceChoice : Window
    {
        public invoiceChoice()
        {
            InitializeComponent();
        }

        private void btnAddExtras_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnGetInvoice_Click(object sender, RoutedEventArgs e)
        {
            
            //If either is empty, return
            if (string.IsNullOrWhiteSpace(txtBookRef.Text) || string.IsNullOrWhiteSpace(txtCustRef.Text))
            {
                MessageBox.Show("Please ensure neither box is empty.");
                return;
            }
            else if (!System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + Convert.ToInt32(txtCustRef.Text)) || !System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + Convert.ToInt32(txtCustRef.Text) + "/" + Convert.ToInt32(txtBookRef.Text)))
            {
                MessageBox.Show("This booking is not affiliated with this customer, please try again");
                return;
            }

            //Create new invoice window to display the invoice
            Invoice invoice = new Invoice();
            invoice.bookRef = Convert.ToInt32(txtBookRef.Text);
            invoice.custRef = Convert.ToInt32(txtCustRef.Text);
            invoice.display();
            invoice.ShowDialog();
        }

        //Only allows number to be input
        private void txtInput_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                e.Handled = true;
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}


