﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * Class for customers
     * Date last modified: 17/11/2016
     * 
     */
    class customerClass
    {
        public string name;
        public string address;

        string path = System.IO.Directory.GetCurrentDirectory();

        public customerClass()
        {

        }

        public void addCust(string nameIn, string addressIn)
        {
            name = nameIn;
            address = addressIn;
        }

        public void writeCust()
        {
            //Assign text to be written to variable text
            string text = name + Environment.NewLine + address;
            int custRef = AutoIncrement.instance.cust_ref;
            //Create file to write to for individual booking references
            System.IO.Directory.CreateDirectory(path + "/" + custRef + "/");
            System.IO.File.WriteAllText(@path + "/" + custRef + "/cust.txt" , text);
            System.IO.File.WriteAllText(@path + "/custNo.txt", Convert.ToString(custRef));

        }

        public static void delCust(int custRef)
        {
            //If directory exists, delete specific booking.
            if (System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + custRef))
            {
                System.IO.Directory.Delete(System.IO.Directory.GetCurrentDirectory() + "/" + custRef, true);
                System.Windows.MessageBox.Show("Customer " + custRef + " has been deleted");
            }
            else
            //If it does not exist, prompt user to reenter data
            {
                System.Windows.MessageBox.Show("Customer " + custRef + " does not exist, please try again.");
            }
        }
    }
}
