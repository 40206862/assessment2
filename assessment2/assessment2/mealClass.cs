﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * class for meals
     * Date last modified: 17/11/2016
     * 
     */
    public class mealClass : extraClass
    {
        public int breakNo;
        public int dinNo;
        public int breakGuestNo;
        public int dinGuestNo;
        public int breakCost;
        public int dinCost;
    }
}
