﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for choosing which extra
     * Date last modified: 17/11/2016
     * 
     */
    public partial class extraChoice : Window
    {
        int bookRef;
        int custRef;
        extraClass extras;
        Factory factory = new Factory();

        public extraChoice()
        {
            InitializeComponent();
        }

        public void extraChoiceCreate(extraClass extrasIn)
        {
            extras = extrasIn;
            bookRef = extrasIn.bookRef;
            custRef = extrasIn.custRef;
        }

        private void btnMeal_Click(object sender, RoutedEventArgs e)
        {
            extras.meals = factory.createMeal(custRef, bookRef);
            extraMeals frmMeals = new extraMeals();
            frmMeals.addMeal(extras.meals);
            frmMeals.ShowDialog();
        }

        private void btnCarHire_Click(object sender, RoutedEventArgs e)
        {
            extras.cars = factory.createCar(custRef, bookRef);
            extraCars frmCars = new extraCars();
            frmCars.addCar(extras.cars);
            frmCars.ShowDialog();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
