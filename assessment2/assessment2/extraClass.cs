﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * class for extras
     * Date last modified: 17/11/2016
     * 
     */
    public class extraClass
    {
        public int bookRef;
        public int custRef;
        public mealClass meals;
        public carClass cars;
    }
}
