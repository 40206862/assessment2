﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    public class bookingClass
    {
     /*
     * Author: Drew Jamieson
     * class for bookings
     * Date last modified: 17/11/2016
     * 
     */
        string arrival;
        string departure;
        int booking_ref = AutoIncrement.instance.booking_ref;
        int days;

        //Find current directory and assign it to path
        string path = System.IO.Directory.GetCurrentDirectory();

        public bookingClass(string departureIn, string arivalIn)
        {
            //Assign the arrival and departure dates as well as the number of days booked
            arrival = arivalIn;
            departure = departureIn;
            //IF DEPARTURE IS < ARRIVAL DON'T DO IT
            days = (Convert.ToDateTime(departure) - Convert.ToDateTime(arrival)).Days;
        }


        public void writeBooking()
        {
            //Assign text to be written to variable text
            string text = arrival + Environment.NewLine + departure + Environment.NewLine + days + Environment.NewLine + booking_ref;
            //Create file to write to for individual booking references
            System.IO.Directory.CreateDirectory(path + "/" + AutoIncrement.instance.cust_ref + "/" + booking_ref);
            System.IO.File.WriteAllText(@path + "/" + AutoIncrement.instance.cust_ref + "/" + booking_ref + "/Booking.txt", text);

            System.IO.File.WriteAllText(@path + "/bookingNo.txt", Convert.ToString(booking_ref));
        }

        public static void delBooking(int bookRef, int custRef)
        {
            //If directory exists, delete specific booking.
            if (System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + custRef + "/" + bookRef))
            {
                System.IO.Directory.Delete(System.IO.Directory.GetCurrentDirectory() + "/" + custRef + "/" + bookRef, true);
                System.Windows.MessageBox.Show("Booking " + bookRef + " for customer " + custRef + " has been deleted");
            }
            else
            //If it does not exist, prompt user to reenter data
            {
                System.Windows.MessageBox.Show("Booking " + bookRef + " for customer " + custRef + " does not exist, please try again.");
            }
        }
    }
}
