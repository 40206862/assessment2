﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class Factory
    {
    /*
     * Author: Drew Jamieson
     * class for extras
     * Date last modified: 17/11/2016
     * Factory to deal with writing, adding, creating and dealing with GUI
     */
        public void addCustomer(string name, string address)
        {
            //Add new customer and save it
            customerClass Customer = new customerClass();
            Customer.addCust(name, address);
            Customer.writeCust();
            
        }
        public void addBooking(string departure, string arrival)
        {
            //Create new booking
            bookingClass Booking =  new bookingClass(departure, arrival);
            Booking.writeBooking();
        }

        public void addGuest(string name, string pass, int age)
        {
            //Create new guest and save it
            guestClass guest = new guestClass(name, pass, age);
            guest.writeGuest();
        }

        public void delBooking(int bookRef, int custRef)
        {
            //Delete booking from files
            bookingClass.delBooking(bookRef, custRef);
        }

        public void delCust(int custRef)
        {
            //Delete customer from files
            customerClass.delCust(custRef);
        }

        public carClass createCar(int custIn, int bookIn)
        { 
            //Return new car
            carClass car = new carClass();
            car.bookRef = bookIn;
            car.custRef = custIn;
            return car;
        }

        public mealClass createMeal(int custIn, int bookIn)
        {
            mealClass meal = new mealClass();
            meal.bookRef = bookIn;
            meal.custRef = custIn;
            return meal;
        }
    }
}
