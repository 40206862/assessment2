﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for displaying the invoice info
     * Date last modified: 17/11/2016
     * 
     */
    public partial class Invoice : Window
    {
        public int bookRef;
        public int custRef;
        public Invoice()
        {
            InitializeComponent();
        }

        public void display()
        {
            //Get current directory
            string path = System.IO.Directory.GetCurrentDirectory();
            //Create new list of strings to store the costs
            string[] text = new string[3];
            text = System.IO.File.ReadAllLines(@path + "/" + custRef + "/" + bookRef + "/cost.txt");

            int i = 0;
            do
            {
                if (String.IsNullOrWhiteSpace(text[i]))
                    text[i] = "0";
                i++;
            } while (i < 4);

                //Label the costs for the booking
                lblCust.Content = "Invoice for customer: " + custRef + " with booking: " + bookRef;
            lblStay.Content = "Cost of staying: £" + text[0];
            lblCar.Content = "Cost of Car Rental: £" + text[1];
            lblBreakfast.Content = "Cost of Breakfasts: £" + text[2];
            lblDinner.Content = "Cost of Dinners: £" + text[3];

           lblTotal.Content = "Total Cost: £" + Convert.ToString((Convert.ToInt32(text[0]) + Convert.ToInt32(text[1]) + Convert.ToInt32(text[2]) + Convert.ToInt32(text[3])));
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
