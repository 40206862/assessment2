﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for extras
     * Date last modified: 17/11/2016
     * 
     */
    public partial class extras : Window
    {
        public extras()
        {
            InitializeComponent();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            //Do not save any extras
            MessageBox.Show("No extras have been added.");
            this.Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddExtras_Click(object sender, RoutedEventArgs e)
        {
            //Create new extras class
            extraClass extras = new extraClass();

            //If either is empty, return
            if (string.IsNullOrWhiteSpace(txtBookRef.Text) || string.IsNullOrWhiteSpace(txtCustRef.Text))
            {
                MessageBox.Show("Please ensure neither box is empty.");
                return;
            }
            else if (!System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + Convert.ToInt32(txtCustRef.Text)) || !System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "/" + Convert.ToInt32(txtCustRef.Text) + "/" + Convert.ToInt32(txtBookRef.Text)))
            {
                MessageBox.Show("This booking is not affiliated with this customer, please try again");
                return;
            }
            //Assign the booking reference number and customer reference number to the booking class
            extras.bookRef = Convert.ToInt32(txtBookRef.Text);
            extras.custRef = Convert.ToInt32(txtCustRef.Text);
            
                
            extraChoice frmExtraChoice = new extraChoice();
            frmExtraChoice.extraChoiceCreate(extras);
            this.Hide();
            frmExtraChoice.ShowDialog();
        }

        //Only allows number to be input
        private void txtInput_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                e.Handled = true;
            }
        }
    }
}
