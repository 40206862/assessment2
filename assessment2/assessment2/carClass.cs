﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * class for cars
     * Date last modified: 17/11/2016
     * 
     */
    public class carClass : extraClass
    {
        public int daysHired;
        public DateTime hireFrom;
        public DateTime hireTo;
        public int cost;
    }
}
