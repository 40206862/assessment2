﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
     /*
      * Author: Drew Jamieson
      * GUI for booking/ammending a booking
      * Date last modified: 17/11/2016
      * 
      */
    public partial class Booking : Window
    {
        Factory factory = new Factory();
        string arrival;
        string departure;
        public bool ammended = false;
        int custRef;
        int bookRef;

        public Booking()
        {
            //Initialise booking
            InitializeComponent();
            //Reference instance of Singleton variable used to store reference numbers
            lblBooking.Content = "Booking Reference Number: " + Convert.ToString((AutoIncrement.instance.booking_ref));
            //Set combobox default dates
            cmbAriveDay.SelectedIndex = 0;
            cmbAriveMonth.SelectedIndex = 0;
            cmbAriveYear.SelectedIndex = 0;
            cmbDepartDay.SelectedIndex = 0;
            cmbDepartMonth.SelectedIndex = 0;
            cmbDepartYear.SelectedIndex = 0;
        }

        public void ammend(int custRefIn, int bookingRefIn)
        {
            InitializeComponent();
            //Reference instance of Singleton variable used to store reference numbers
            lblBooking.Content = "Booking Reference Number: " + Convert.ToString(bookingRefIn);
            //Set path to find original copies
            string path = System.IO.Directory.GetCurrentDirectory();

            string text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + custRefIn + "/" + bookingRefIn + "/Booking.txt").Skip(0).Take(1).First());

            //Set combobox default dates
            string[] date = new string[3];
            int i=0;
            foreach (string word in text.Split(' '))
            {
                date[i] = word;
                i++;
            }
            cmbAriveDay.Text = date[0];
            cmbAriveMonth.Text = date[1];
            cmbAriveYear.Text = date[2];

            text = Convert.ToString(System.IO.File.ReadLines(@path + "/" + custRefIn + "/" + bookingRefIn + "/Booking.txt").Skip(1).Take(1).First());
            i = 0;
            foreach (string word in text.Split(' '))
            {
                date[i] = word;
                i++;
            }
            cmbDepartDay.Text = date[0];
            cmbDepartMonth.Text = date[1];
            cmbDepartYear.Text = date[2];

            //Hide Continue button
            btnSaveAndExit.Visibility = Visibility.Hidden;

            custRef = custRefIn;
            bookRef = bookingRefIn;
            ammended = true;
        }

        private void cmbAriveDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void comboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbAriveYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnSaveAndExit_Click(object sender, RoutedEventArgs e)
        {
            //Assign departure and arrival dates
            departure = Convert.ToInt32(cmbDepartDay.Text) + " " + Convert.ToString(cmbDepartMonth.Text) + " " + Convert.ToInt32(cmbDepartYear.Text);
            arrival = Convert.ToInt32(cmbAriveDay.Text) + " " + Convert.ToString(cmbAriveMonth.Text) + " " + Convert.ToInt32(cmbAriveYear.Text);

            //If the length of staying is equal to or less than 0
            if ((Convert.ToDateTime(departure) - Convert.ToDateTime(arrival)).Days <= 0)
            {
                System.Windows.MessageBox.Show("This is not a valid date, please try again.");
                return;
            }

            //Create new booking, save booking and close form
            
            factory.addBooking(departure, arrival);
            frmBooking.Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            //Inform user that no changes have been saved and exit to options menu
            MessageBox.Show("No booking has been booked");
            this.Close();
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            
            //Assign departure and arrival dates
            departure = Convert.ToInt32(cmbDepartDay.Text) + " " + Convert.ToString(cmbDepartMonth.Text) + " " + Convert.ToInt32(cmbDepartYear.Text);
            arrival = Convert.ToInt32(cmbAriveDay.Text) + " " + Convert.ToString(cmbAriveMonth.Text) + " " + Convert.ToInt32(cmbAriveYear.Text);

            //If the length of staying is equal to or less than 0
            if ((Convert.ToDateTime(departure) - Convert.ToDateTime(arrival)).Days <= 0)
            {
                System.Windows.MessageBox.Show("This is not a valid date, please try again.");
                return;
            }
            //Add new booking
            factory.addBooking(departure, arrival);
            Guest guest = new Guest();
            if (ammended)
            {
                //If this has been ammended then make sure guests is still the same
                MessageBox.Show("Please ensure guests have remained the same");
                guest.ammend(custRef, bookRef);
            }
            this.Close();
            guest.ShowDialog();
        }
    }
}
