﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * GUI for creating new guest
     * Date last modified: 17/11/2016
     * 
     */
    public partial class Guest : Window
    {
        bool select = false;



        public void ammend(int custRefIn, int bookingRefIn)
        {
            string path = System.IO.Directory.GetCurrentDirectory();
            select = true;
            int CASE;

            if (System.IO.Directory.Exists(path + "/" + custRefIn + "/" + bookingRefIn + "/guests/"))
            {
                CASE = (System.IO.Directory.GetFiles(path + "/" + custRefIn + "/" + bookingRefIn + "/guests/").Length);
            }
            else
            {
                MessageBox.Show("No guests exist under this booking reference number, please create new guest.");
                return;
            }

            string[] file = new string[4];
            int i = 0;

            //For each file in the directory, assign it's name and path to file[i]
            foreach (string name in System.IO.Directory.GetFiles(path + "/" + custRefIn + "/" + bookingRefIn + "/guests/"))
            {
                file[i] = name;
                i++;
            }
            //Set i to 0 to allow for files to be read
            i = 0;

            btnQuit.Visibility = Visibility.Hidden;

            //Only show and fill the boxes relevant to the number of guests 
            if (CASE >= 1)
            {
                lblGuestAge1.Visibility = Visibility.Visible;
                lblGuestName1.Visibility = Visibility.Visible;
                lblGuestPass1.Visibility = Visibility.Visible;
                txtGuestName1.Visibility = Visibility.Visible;
                txtGuestPass1.Visibility = Visibility.Visible;
                cmbGuestAge1.Visibility = Visibility.Visible;
                noSelected = 1;

                txtGuestName1.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(0).Take(1).First());
                txtGuestPass1.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(1).Take(1).First());
                cmbGuestAge1.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(2).Take(1).First());
                i++;                


                if (CASE >= 2)
                {
                    lblGuestAge2.Visibility = Visibility.Visible;
                    lblGuestName2.Visibility = Visibility.Visible;
                    lblGuestPass2.Visibility = Visibility.Visible;
                    txtGuestName2.Visibility = Visibility.Visible;
                    txtGuestPass2.Visibility = Visibility.Visible;
                    cmbGuestAge2.Visibility = Visibility.Visible;
                    noSelected = 2;

                    txtGuestName2.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(0).Take(1).First());
                    txtGuestPass2.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(1).Take(1).First());
                    cmbGuestAge2.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(2).Take(1).First());
                    i++;
                    if (CASE >= 3)
                    {
                        lblGuestAge3.Visibility = Visibility.Visible;
                        lblGuestName3.Visibility = Visibility.Visible;
                        lblGuestPass3.Visibility = Visibility.Visible;
                        txtGuestName3.Visibility = Visibility.Visible;
                        txtGuestPass3.Visibility = Visibility.Visible;
                        cmbGuestAge3.Visibility = Visibility.Visible;
                        noSelected = 3;

                        txtGuestName3.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(0).Take(1).First());
                        txtGuestPass3.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(1).Take(1).First());
                        cmbGuestAge3.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(2).Take(1).First());
                        i++;
                        if (CASE >= 4)
                        {
                            lblGuestAge4.Visibility = Visibility.Visible;
                            lblGuestName4.Visibility = Visibility.Visible;
                            lblGuestPass4.Visibility = Visibility.Visible;
                            txtGuestName4.Visibility = Visibility.Visible;
                            txtGuestPass4.Visibility = Visibility.Visible;
                            cmbGuestAge4.Visibility = Visibility.Visible;
                            noSelected = 4;

                            txtGuestName4.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(0).Take(1).First());
                            txtGuestPass4.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(1).Take(1).First());
                            cmbGuestAge4.Text = Convert.ToString(System.IO.File.ReadLines(file[i]).Skip(2).Take(1).First());
                        }
                    }
                }
            }
        }
        public Guest()
        {
            InitializeComponent();
            cmbGuestNo.SelectedIndex = 0;
            cmbGuestAge1.SelectedIndex = 0;
            cmbGuestAge2.SelectedIndex = 0;
            cmbGuestAge3.SelectedIndex = 0;
            cmbGuestAge4.SelectedIndex = 0;

            //Start with all options hidden
            cmbGuestAge1.Visibility = Visibility.Hidden;
            cmbGuestAge2.Visibility = Visibility.Hidden;
            cmbGuestAge3.Visibility = Visibility.Hidden;
            cmbGuestAge4.Visibility = Visibility.Hidden;

            txtGuestName1.Visibility = Visibility.Hidden;
            txtGuestName2.Visibility = Visibility.Hidden;
            txtGuestName3.Visibility = Visibility.Hidden;
            txtGuestName4.Visibility = Visibility.Hidden;

            txtGuestPass1.Visibility = Visibility.Hidden;
            txtGuestPass2.Visibility = Visibility.Hidden;
            txtGuestPass3.Visibility = Visibility.Hidden;
            txtGuestPass4.Visibility = Visibility.Hidden;

            lblGuestAge1.Visibility = Visibility.Hidden;
            lblGuestAge2.Visibility = Visibility.Hidden;
            lblGuestAge3.Visibility = Visibility.Hidden;
            lblGuestAge4.Visibility = Visibility.Hidden;

            lblGuestName1.Visibility = Visibility.Hidden;
            lblGuestName2.Visibility = Visibility.Hidden;
            lblGuestName3.Visibility = Visibility.Hidden;
            lblGuestName4.Visibility = Visibility.Hidden;

            lblGuestPass1.Visibility = Visibility.Hidden;
            lblGuestPass2.Visibility = Visibility.Hidden;
            lblGuestPass3.Visibility = Visibility.Hidden;
            lblGuestPass4.Visibility = Visibility.Hidden;

            lblGuestAge1.Visibility = Visibility.Hidden;
            lblGuestAge2.Visibility = Visibility.Hidden;
            lblGuestAge3.Visibility = Visibility.Hidden;
            lblGuestAge4.Visibility = Visibility.Hidden;

            cmbGuestAge1.Visibility = Visibility.Hidden;
            cmbGuestAge2.Visibility = Visibility.Hidden;
            cmbGuestAge3.Visibility = Visibility.Hidden;
            cmbGuestAge4.Visibility = Visibility.Hidden;
        }
        private int noSelected;
        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void cmbGuestAge1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cmbGuestNo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          
        }


        private void txtGuestPass1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtGuestName2_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void textBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void btSelect_Click(object sender, RoutedEventArgs e)
        {
            hideAll();
            select = true;
            int CASE = Convert.ToInt32(cmbGuestNo.Text);
            //Only show the boxes relevant to the number of guests
            if (CASE >= 1)
            {
                lblGuestAge1.Visibility = Visibility.Visible;
                lblGuestName1.Visibility = Visibility.Visible;
                lblGuestPass1.Visibility = Visibility.Visible;
                txtGuestName1.Visibility = Visibility.Visible;
                txtGuestPass1.Visibility = Visibility.Visible;
                cmbGuestAge1.Visibility = Visibility.Visible;
                noSelected = 1;
                if (CASE >= 2)
                {
                    lblGuestAge2.Visibility = Visibility.Visible;
                    lblGuestName2.Visibility = Visibility.Visible;
                    lblGuestPass2.Visibility = Visibility.Visible;
                    txtGuestName2.Visibility = Visibility.Visible;
                    txtGuestPass2.Visibility = Visibility.Visible;
                    cmbGuestAge2.Visibility = Visibility.Visible;
                    noSelected = 2;
                    if (CASE >= 3)
                    {
                        lblGuestAge3.Visibility = Visibility.Visible;
                        lblGuestName3.Visibility = Visibility.Visible;
                        lblGuestPass3.Visibility = Visibility.Visible;
                        txtGuestName3.Visibility = Visibility.Visible;
                        txtGuestPass3.Visibility = Visibility.Visible;
                        cmbGuestAge3.Visibility = Visibility.Visible;
                        noSelected = 3;
                        if (CASE >= 4)
                        {
                            lblGuestAge4.Visibility = Visibility.Visible;
                            lblGuestName4.Visibility = Visibility.Visible;
                            lblGuestPass4.Visibility = Visibility.Visible;
                            txtGuestName4.Visibility = Visibility.Visible;
                            txtGuestPass4.Visibility = Visibility.Visible;
                            cmbGuestAge4.Visibility = Visibility.Visible;
                            noSelected = 4;
                        }
                    }
                }            
            }
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            //Do not save any changes
            MessageBox.Show("No guest has been saved.");
            this.Close();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Empty all textboxes
            txtGuestName1.Clear();
            txtGuestName2.Clear();
            txtGuestName3.Clear();
            txtGuestName4.Clear();

            txtGuestPass1.Clear();
            txtGuestPass2.Clear();
            txtGuestPass3.Clear();
            txtGuestPass4.Clear();
        }

        private void btnSaveQuit_Click(object sender, RoutedEventArgs e)
        {
            if (!select)
            {
                MessageBox.Show("Please select the number of guests");
                return;
            }
            Factory factory = new Factory();
            //Add guests for the relevant number of guests to be added
            if (noSelected >= 1)
            {
                factory.addGuest(txtGuestName1.Text, txtGuestPass1.Text, Convert.ToInt32(cmbGuestAge1.Text));
                if (noSelected >= 2)
                {
                    factory.addGuest(txtGuestName2.Text, txtGuestPass2.Text, Convert.ToInt32(cmbGuestAge2.Text));
                    if (noSelected >= 3)
                    {
                        factory.addGuest(txtGuestName3.Text, txtGuestPass3.Text, Convert.ToInt32(cmbGuestAge3.Text));
                        if (noSelected >= 4)
                        {
                            factory.addGuest(txtGuestName4.Text, txtGuestPass4.Text, Convert.ToInt32(cmbGuestAge4.Text));
                        }
                    }
                }
            }
            this.Close();
        }

        private void hideAll()
        {
            lblGuestAge1.Visibility = Visibility.Hidden;
            lblGuestName1.Visibility = Visibility.Hidden;
            lblGuestPass1.Visibility = Visibility.Hidden;
            txtGuestName1.Visibility = Visibility.Hidden;
            txtGuestPass1.Visibility = Visibility.Hidden;
            cmbGuestAge1.Visibility = Visibility.Hidden;

            lblGuestAge2.Visibility = Visibility.Hidden;
            lblGuestName2.Visibility = Visibility.Hidden;
            lblGuestPass2.Visibility = Visibility.Hidden;
            txtGuestName2.Visibility = Visibility.Hidden;
            txtGuestPass2.Visibility = Visibility.Hidden;
            cmbGuestAge2.Visibility = Visibility.Hidden;

            lblGuestAge3.Visibility = Visibility.Hidden;
            lblGuestName3.Visibility = Visibility.Hidden;
            lblGuestPass3.Visibility = Visibility.Hidden;
            txtGuestName3.Visibility = Visibility.Hidden;
            txtGuestPass3.Visibility = Visibility.Hidden;
            cmbGuestAge3.Visibility = Visibility.Hidden;

            lblGuestAge4.Visibility = Visibility.Hidden;
            lblGuestName4.Visibility = Visibility.Hidden;
            lblGuestPass4.Visibility = Visibility.Hidden;
            txtGuestName4.Visibility = Visibility.Hidden;
            txtGuestPass4.Visibility = Visibility.Hidden;
            cmbGuestAge4.Visibility = Visibility.Hidden;
        }
    }
}
