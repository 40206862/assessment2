﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /*
     * Author: Drew Jamieson
     * gui for adding the extras
     * Date last modified: 17/11/2016
     * 
     */
    public partial class extraMeals : Window
    {
        mealClass meal;
        Factory factory = new Factory();
        string path = System.IO.Directory.GetCurrentDirectory();


        public extraMeals()
        {
            InitializeComponent();
        }

        public void addMeal(mealClass mealsIn)
        {
            meal = mealsIn;

            //Only allow a booking for up to the number of guests
            int i = 0;
            do
            {
                this.cmbBreakGuest.Items.Add(Convert.ToString(i));
                this.cmbDinGuest.Items.Add(Convert.ToString(i));
                i++;
            } while (i < ((System.IO.Directory.GetFiles(path + "/" + meal.custRef + "/" + meal.bookRef + "/guests/")).Length)+1);



            //Only allow a booking for up to the number of nights stayed
            i = 0;
            do
            {
                this.cmbBreakNo.Items.Add(Convert.ToString(i));
                this.cmbDinNo.Items.Add(Convert.ToString(i));
                i++;
            } while (i < (Convert.ToInt32(System.IO.File.ReadAllLines(path + "/" + meal.custRef + "/" + meal.bookRef + "/Booking.txt").Skip(2).Take(1).First())) + 1);

            cmbBreakGuest.SelectedIndex = 0;
            cmbDinGuest.SelectedIndex = 0;
            cmbBreakNo.SelectedIndex = 0;
            cmbDinNo.SelectedIndex = 0;


        }

        


        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("No changes have been made");
            this.Close();
        }

        private void cmbDinNo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        

        private void btnAddBreak1_Click(object sender, RoutedEventArgs e)
        {
            addBreak();
        }

        private void btnAddDin_Click(object sender, RoutedEventArgs e)
        {
            addDinner();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            addBreak();
            addDinner();
            this.Close();
        }


        private void addBreak()
        {
            meal.breakGuestNo = Convert.ToInt32(cmbBreakGuest.Text);
            meal.breakNo = Convert.ToInt32(cmbBreakNo.Text);
            meal.breakCost = meal.breakNo * meal.breakGuestNo * 5;

            //Add the cost of the breakfast meal to cost.txt
            string[] text = System.IO.File.ReadAllLines(@path + "/" + meal.custRef + "/" + meal.bookRef + "/cost.txt");
            text[2] = Convert.ToString(meal.breakCost);
            System.IO.File.WriteAllLines(@path + "/" + meal.custRef + "/" + meal.bookRef + "/cost.txt", text);

            //If dietary req isn't empty, create file and store it
            if (!string.IsNullOrWhiteSpace(txtBreakReq.Text))
            {
                //Create a file for the requirements 
                string eatingReq = txtBreakReq.Text;
                if (!System.IO.Directory.Exists(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals"))
                    System.IO.Directory.CreateDirectory(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals");
                System.IO.File.WriteAllText(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals/breakfast.txt", eatingReq);
            }
        }

        private void addDinner()
        {
            meal.dinGuestNo = Convert.ToInt32(cmbDinGuest.Text);
            meal.dinNo = Convert.ToInt32(cmbDinNo.Text);
            meal.dinCost = meal.dinNo * meal.dinGuestNo * 15;

            //Add the cost of the dinner meal to cost.txt
            string[] text = System.IO.File.ReadAllLines(@path + "/" + meal.custRef + "/" + meal.bookRef + "/cost.txt");
            text[3] = Convert.ToString(meal.dinCost);
            
            System.IO.File.WriteAllLines(@path + "/" + meal.custRef + "/" + meal.bookRef + "/cost.txt", text);

            if (!string.IsNullOrWhiteSpace(txtDinReq.Text))
            {
                //Create a file for the requirements 
                string eatingReq = txtDinReq.Text;
                if (!System.IO.Directory.Exists(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals/"))
                    System.IO.Directory.CreateDirectory(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals");
                System.IO.File.WriteAllText(@path + "/" + meal.custRef + "/" + meal.bookRef + "/meals/dinner.txt", eatingReq);
            }
        }
    }
}
